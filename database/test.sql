-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2019 at 01:18 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  `price` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `name`, `path`, `price`) VALUES
(1, 'Cannon EOS 77D', 'img/camera.jpg', 55000),
(2, 'Panasonic Lumix', 'img/panasonic.jpg', 45000),
(3, 'Cannon EDS 99PPi', 'img/camera.jpg', 43000),
(4, 'Panasonic 100LI', 'img/panasonic.jpg', 35000),
(5, 'Sewor Antique', 'img/watch2.jpg', 10000),
(6, 'YK Glance', 'img/watch.jpg', 7300),
(7, 'Huaweii GT', 'img/watch3.jpg', 13000),
(8, 'Audemars Piguet', 'img/watch4.jpg', 6799),
(9, 'Peter England Cotton', 'img/shirt.jpg', 1600),
(10, 'Arrow Slim Fit', 'img/shirt2.jpg', 2700),
(11, 'Tommy Synthetic', 'img/shirt3.jpg', 2100),
(12, 'Gucci HackCotton', 'img/shirt4.jpg', 4100);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `phone` decimal(10,0) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `passw` varchar(255) NOT NULL,
  `registration_stamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `first_name`, `phone`, `city`, `address`, `passw`, `registration_stamp`) VALUES
(1, 'venu@xyz.com', 'Venu', NULL, 'Sharma', '', '547903927', '2016-09-28 21:30:04'),
(2, 'shubham@xyz.com', 'Shubham', NULL, NULL, '', '538915694', '2019-06-14 00:46:16'),
(3, 'disha@xyz.com', 'Disha', NULL, 'Kaur', '', '557825323', '2016-09-30 00:02:14'),
(4, 'ankit@xyz.com', 'Ankit', NULL, 'Kumar', '', '561322116', '2016-09-30 15:05:18'),
(5, 'mrinal@xyz.com', 'Mrinal', NULL, 'Joy', '', '517918670', '2016-10-02 09:08:06'),
(6, 'abhilash@xyz.com', 'Abhilash', NULL, 'Jalsani', '', '509841902', '2016-10-01 10:30:00'),
(7, 'hardik@xyz.com', 'Hardik', NULL, 'Arora', '', 'Jawwad', '2016-09-30 13:20:45'),
(8, 'yesha@xyz.com', 'Yesha', NULL, 'Krishna', '', '534532216', '2016-09-30 13:20:45'),
(9, 'rushit@xyz.com', 'Rushit', NULL, NULL, '', '534359370', '2016-09-29 11:46:37'),
(10, 'jessy@xyz.com', 'Jessy', NULL, 'Joseph', '', '591053100', '2016-09-29 00:02:14'),
(11, 'jasper@xyz.com', 'Jaspreet', NULL, NULL, '', '515078235', '2016-09-29 23:50:12'),
(12, 'prachi@xyz.com', 'Prachi', NULL, NULL, '', '530670640', '2016-09-29 12:12:12'),
(13, 'jawwadayyubi15@gmail.com', 'Jawwad', NULL, 'Ayyubi', '', '1234', '2019-06-26 16:37:52'),
(14, 'jaw@hhh.com', 'jaw', NULL, '', '', 'root', '2019-06-27 13:49:08'),
(15, 'aamnazahra@gmail.com', 'Aamna', NULL, '', '', '803d3497f5d96db7d20c9d874c3fa3c3', '2019-06-27 14:01:07'),
(16, 'soubiazahra@gmail.com', 'soubia', NULL, 'zahra', '', '561da451d0530c2b17bb0758784e4093', '2019-06-28 11:03:51'),
(17, 'jawwadayyubi.amu@gmail.com', 'Jawwad', NULL, 'Ayyubi', '', 'f429c2bb3ff3dfdde7550d2b426d984a', '2019-07-11 15:49:04'),
(18, 'kaali@gmail.com', 'Kaali', '1345534611', '', '', '3ca314ab84358834b06180c4e8217eab', '2019-07-18 15:35:15'),
(19, 'kalla@gmail', 'kalla', '0', '', '', '70a2bd7231f3cd204b11462139fdc2bc', '2019-07-18 15:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `users_items`
--

CREATE TABLE `users_items` (
  `id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `purchase_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_items`
--

INSERT INTO `users_items` (`id`, `user_id`, `item_id`, `purchase_timestamp`, `stat`) VALUES
(17, 16, 2, '2019-06-28 11:39:53', 'Added to cart'),
(18, 16, 2, '2019-06-28 11:39:57', 'Added to cart'),
(19, 16, 7, '2019-06-28 11:40:21', 'Added to cart'),
(20, 16, 1, '2019-06-28 11:42:01', 'Added to cart'),
(21, 16, 1, '2019-06-28 11:43:33', 'Added to cart'),
(22, 16, 3, '2019-06-28 11:44:41', 'Added to cart'),
(23, 16, 9, '2019-06-28 11:47:38', 'Added to cart'),
(26, 16, 10, '2019-06-28 11:47:53', 'Added to cart'),
(28, 15, 5, '2019-06-28 11:50:25', 'Confirmed'),
(39, 17, 8, '2019-07-11 18:20:32', 'Confirmed'),
(41, 17, 9, '2019-07-11 18:20:37', 'Confirmed'),
(45, 17, 5, '2019-07-11 23:57:02', 'Confirmed'),
(49, 17, 4, '2019-07-11 23:57:29', 'Confirmed'),
(50, 15, 8, '2019-07-18 14:46:50', 'Confirmed'),
(51, 15, 12, '2019-07-18 14:46:55', 'Confirmed'),
(52, 15, 4, '2019-07-18 14:49:15', 'Confirmed'),
(54, 15, 6, '2019-07-18 15:15:08', 'Confirmed'),
(55, 19, 2, '2019-07-18 15:37:38', 'Added to cart'),
(57, 15, 9, '2019-07-21 16:20:52', 'Confirmed'),
(58, 15, 4, '2019-07-21 16:21:09', 'Confirmed');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_items`
--
ALTER TABLE `users_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`item_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users_items`
--
ALTER TABLE `users_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_items`
--
ALTER TABLE `users_items`
  ADD CONSTRAINT `users_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `users_items_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
